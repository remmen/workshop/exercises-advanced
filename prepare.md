# Prepare your environment

## Gitlab
- Create an account under https://gitlab.com/users/sign_up
- Create a new public project under https://gitlab.com/projects/new
- Copy the link and clone the git repository with "git clone <url>

##Tools
- Install kubectl for your environment https://kubernetes.io/docs/tasks/tools/
- Install talosctl https://github.com/siderolabs/talos/releases

## Create your kubernetes cluster
- Make sure your docker environment is running
- Create a new cluster from scratch with `talosctl cluster create`
- Verify your cluster `talsoctl cluster show`
- Access your cluster with kubectl `kubectl get nodes`



