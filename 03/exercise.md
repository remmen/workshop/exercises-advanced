# Create a simple cache

Extend the existing .gitlab-ci.yml configuration by passing the output of the build_app job as artifact to the app_build_container job.

- The output of the application is stored under `app/.output/public`