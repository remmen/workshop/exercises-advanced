# Create a simple cache

Extend the existing .gitlab-ci.yml configuration by adding a simple cache.

- The first job should save the current timestamp into a file using `date`
- The other jobs should read the cache an output it to the console

- Run the pipelines multiple times and compare the timestamps