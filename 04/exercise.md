# Custom order using `needs`

Extend the existing .gitlab-ci.yml configuration creating the following order.

- e2e test depends on the two build jobs
- deploy depends on the e2e tests