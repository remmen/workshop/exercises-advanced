# Create a simple cache

Extend the existing .gitlab-ci.yml configuration by adding a simple cache.

- As key use the file `package-lock.json`
- In order to activate the cache for selected jobs, create a hidden job to be used with extends


