# Rest API

Create a token and retrieve the following information fromt the REST API

1) Your Project information 
    curl -s --header "PRIVATE-TOKEN: XXX" https://gitlab.com/api/v4/projects/ID

2) List of recents CI/CD Jobs
    curl -s --header "PRIVATE-TOKEN: XXX" https://gitlab.com/api/v4/projects/ID/jobs 

3) Duration of a specific Job 
    curl -s --header "PRIVATE-TOKEN: XXX" https://gitlab.com/api/v4/projects/ID/jobs | jq -r '.[] | select(.name == "depenencies") | "\(.name): \(.duration) seconds"'
